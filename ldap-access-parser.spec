Name:           ldap-access-parser
Version:        %{getenv:RPM_VERSION}
Release:        %{getenv:RPM_RELEASE}%{?dist}
Summary:        ldap access log parser

License:        MIT
URL:            https://gitlab.cern.ch/qduponno/ldap-access-parser
Source0: %{name}-%{version}.tar.gz    

#BuildRequires:  golang
#BuildRequires:  systemd-rpm-macros

Provides:       %{name} = %{version}

%description
A simple web app

%global debug_package %{nil}

%prep
%autosetup


%build
go build -v -o %{name}


%install

install -Dpm 0755 %{name} %{buildroot}%{_bindir}/%{name}
install -Dpm 644 %{name}.service %{buildroot}%{_unitdir}/%{name}.service

%check
# go test should be here... :)

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%{_bindir}/%{name}
%{_unitdir}/%{name}.service